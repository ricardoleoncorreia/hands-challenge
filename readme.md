# Hands Challenge

This is the solution for a variant of the challenge in this [repo](https://github.com/jesus-seijas-sp/hand-challenge).

# Requirements

* Use TDD with Typescript and Jest.
* A translator function should be built.

# How to run the tests?

* Install dependencies with `npm install`.
* Execute the tests with `npm test`.
