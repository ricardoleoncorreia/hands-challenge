export type Hand = '👉' | '👈' | '👆' | '👇' | '🤜' | '🤛' | '👊';
export type ActionsMapper = {
  [key in Hand]: () => void;
};
export type LoopIndexMapper = {
  [key: string]: number
};
