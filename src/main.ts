import { ActionsMapper, Hand, LoopIndexMapper } from "./main.interfaces";

function loopIndexMapperGenerator(instructions: Hand[]): LoopIndexMapper {
  const openLoopIndexes: number[] = [];
  const loopIndexMapper: LoopIndexMapper = {};

  instructions.forEach((hand: Hand, index: number) => {
    if (hand === '🤜') openLoopIndexes.push(index);
    if (hand === '🤛') {
      const openPosition = openLoopIndexes.pop();
      if(!openPosition) throw new Error('No match for current close instruction was found');

      loopIndexMapper[openPosition] = index;
      loopIndexMapper[index] = openPosition;
    }
  });

  return loopIndexMapper;
}

export function handLanguageTranslator(handText: string): string {
  const instructions = Array.from(handText) as Hand[];
  const loopIndexMapper = loopIndexMapperGenerator(instructions);
  const memory: number[] = [0];
  let memoryIndex = 0;
  let instructionIndex = 0;
  let message = '';

  const actionsMapper: ActionsMapper = {
    '👉': () => {
      memoryIndex++;
      memory[memoryIndex] ??= 0;
    },
    '👈': () => memoryIndex--,
    '👆': () => memory[memoryIndex] = memory[memoryIndex] === 255 ? 0 : memory[memoryIndex] + 1,
    '👇': () => memory[memoryIndex] = memory[memoryIndex] === 0 ? 255 : memory[memoryIndex] - 1,
    '🤜': () => {
      if (memory[memoryIndex] === 0) {
        instructionIndex = loopIndexMapper[instructionIndex];
      }
    },
    '🤛': () => {
      if (memory[memoryIndex] !== 0) {
        instructionIndex = loopIndexMapper[instructionIndex];
      }
    },
    '👊': () => message += String.fromCharCode(memory[memoryIndex]),
  };

  while (instructionIndex < instructions.length) {
    const hand = instructions[instructionIndex] as Hand;
    actionsMapper[hand]();
    instructionIndex++;
  }

  return message;
}
